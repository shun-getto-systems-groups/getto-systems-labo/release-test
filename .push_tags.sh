#!/bin/bash

git remote add super https://getto-systems:$GITLAB_ACCESS_TOKEN@gitlab.com/getto-systems-labo/release-test.git
git tag $(cat .release-version)
git push super HEAD:master --tags
